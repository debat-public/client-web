import gql from 'graphql-tag';
import * as ApolloReactCommon from '@apollo/react-common';
import * as React from 'react';
import * as ApolloReactComponents from '@apollo/react-components';
import * as ApolloReactHoc from '@apollo/react-hoc';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: any }> = { [K in keyof T]: T[K] };
export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

export type Role = {
  __typename?: 'Role';
  id?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
};

export type ListUsers = {
  __typename?: 'ListUsers';
  id_role?: Maybe<Scalars['String']>;
  id_user?: Maybe<Scalars['String']>;
};

export type User = {
  __typename?: 'User';
  age?: Maybe<Scalars['String']>;
  bio?: Maybe<Scalars['String']>;
  email?: Maybe<Scalars['String']>;
  firstname?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
  job?: Maybe<Scalars['String']>;
  lastname?: Maybe<Scalars['String']>;
  password?: Maybe<Scalars['String']>;
  roles?: Maybe<Array<Maybe<Role>>>;
};

export type Reaction = {
  __typename?: 'Reaction';
  created_at?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
  title?: Maybe<Scalars['String']>;
};

export type Opinion = {
  __typename?: 'Opinion';
  created_at?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
  title?: Maybe<Scalars['String']>;
};

/** List of mutation, to validate and store data */
export type Mutation = {
  __typename?: 'Mutation';
  CreateBlock?: Maybe<Block>;
  /** return new opinion and create relationship with user */
  CreateOpinion?: Maybe<Opinion>;
  /** return new project, create relationship with user and list of block */
  CreateProject?: Maybe<Project>;
  /** return new reaction and create relationship with user */
  CreateReaction?: Maybe<Reaction>;
  CreateRole?: Maybe<Role>;
  CreateUser?: Maybe<User>;
  UserRole?: Maybe<ListUsers>;
};


/** List of mutation, to validate and store data */
export type MutationCreateBlockArgs = {
  title: Scalars['String'];
  body: Scalars['String'];
  index: Scalars['Int'];
};


/** List of mutation, to validate and store data */
export type MutationCreateOpinionArgs = {
  title: Scalars['String'];
  description: Scalars['String'];
  idUser: Scalars['String'];
  idProject: Scalars['String'];
};


/** List of mutation, to validate and store data */
export type MutationCreateProjectArgs = {
  title: Scalars['String'];
  description: Scalars['String'];
  idUser: Scalars['String'];
  listBlocks?: Maybe<Array<Maybe<BlockInput>>>;
};


/** List of mutation, to validate and store data */
export type MutationCreateReactionArgs = {
  title: Scalars['String'];
  idUser: Scalars['String'];
  idProject: Scalars['String'];
};


/** List of mutation, to validate and store data */
export type MutationCreateRoleArgs = {
  name: Scalars['String'];
};


/** List of mutation, to validate and store data */
export type MutationCreateUserArgs = {
  firstname: Scalars['String'];
  lastname: Scalars['String'];
};


/** List of mutation, to validate and store data */
export type MutationUserRoleArgs = {
  id_user: Scalars['String'];
  id_role: Scalars['String'];
};

export type BlockInput = {
  title?: Maybe<Scalars['String']>;
  body?: Maybe<Scalars['String']>;
  index?: Maybe<Scalars['Int']>;
};

/** List of queries, to validate and execute, retrieve data */
export type Query = {
  __typename?: 'Query';
  /** return list block/projet */
  ListBlocks?: Maybe<Array<Maybe<ListsBlock>>>;
  /** return list of opinion */
  ListOpinions?: Maybe<Array<Maybe<Opinion>>>;
  /** return list of project/user */
  ListProjects?: Maybe<Array<Maybe<ListsProject>>>;
  /** return list of reactions */
  ListReactions?: Maybe<Array<Maybe<Reaction>>>;
  /** return list of user by role */
  ListUser?: Maybe<Array<Maybe<ListUsers>>>;
  /** Return Opinion from ID */
  Opinion?: Maybe<Opinion>;
  /** Return Project from ID */
  Project?: Maybe<Project>;
  /** Return Role from ID */
  Role?: Maybe<Role>;
  /** Return User from ID */
  User?: Maybe<User>;
  provideUser?: Maybe<User>;
};


/** List of queries, to validate and execute, retrieve data */
export type QueryListBlocksArgs = {
  by: Scalars['String'];
  id?: Maybe<Scalars['String']>;
  offset?: Maybe<Scalars['Int']>;
  limit?: Maybe<Scalars['Int']>;
};


/** List of queries, to validate and execute, retrieve data */
export type QueryListOpinionsArgs = {
  by: Scalars['String'];
  id?: Maybe<Scalars['String']>;
  offset?: Maybe<Scalars['Int']>;
  limit?: Maybe<Scalars['Int']>;
};


/** List of queries, to validate and execute, retrieve data */
export type QueryListProjectsArgs = {
  offset?: Maybe<Scalars['Int']>;
  limit?: Maybe<Scalars['Int']>;
  byIdCategory?: Scalars['String'];
  id?: Maybe<Scalars['String']>;
};


/** List of queries, to validate and execute, retrieve data */
export type QueryListReactionsArgs = {
  by: Scalars['String'];
  id?: Maybe<Scalars['String']>;
  offset?: Maybe<Scalars['Int']>;
  limit?: Maybe<Scalars['Int']>;
};


/** List of queries, to validate and execute, retrieve data */
export type QueryListUserArgs = {
  byRole?: Scalars['String'];
  id?: Maybe<Scalars['String']>;
  offset?: Maybe<Scalars['Int']>;
  limit?: Maybe<Scalars['Int']>;
};


/** List of queries, to validate and execute, retrieve data */
export type QueryOpinionArgs = {
  id: Scalars['String'];
};


/** List of queries, to validate and execute, retrieve data */
export type QueryProjectArgs = {
  id: Scalars['String'];
};


/** List of queries, to validate and execute, retrieve data */
export type QueryRoleArgs = {
  id: Scalars['String'];
};


/** List of queries, to validate and execute, retrieve data */
export type QueryUserArgs = {
  id: Scalars['String'];
};


/** List of queries, to validate and execute, retrieve data */
export type QueryProvideUserArgs = {
  provider?: Maybe<Scalars['String']>;
  code?: Maybe<Scalars['String']>;
  state?: Maybe<Scalars['String']>;
};

export type ListsBlock = {
  __typename?: 'listsBlock';
  id_block?: Maybe<Scalars['String']>;
  id_project?: Maybe<Scalars['String']>;
};

export type ListsProject = {
  __typename?: 'ListsProject';
  ID_user?: Maybe<Scalars['String']>;
  id_project?: Maybe<Scalars['String']>;
};

export type Project = {
  __typename?: 'Project';
  created_at?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
  title?: Maybe<Scalars['String']>;
};

export type Block = {
  __typename?: 'Block';
  body?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
  index?: Maybe<Scalars['Int']>;
  title?: Maybe<Scalars['String']>;
};

export type CreateOpinionMutationVariables = Exact<{
  title: Scalars['String'];
  description: Scalars['String'];
  idUser: Scalars['String'];
  idProject: Scalars['String'];
}>;


export type CreateOpinionMutation = (
  { __typename?: 'Mutation' }
  & { CreateOpinion?: Maybe<(
    { __typename?: 'Opinion' }
    & Pick<Opinion, 'title' | 'description'>
  )> }
);

export type CreateReactionMutationVariables = Exact<{
  title: Scalars['String'];
  idUser: Scalars['String'];
  idProject: Scalars['String'];
}>;


export type CreateReactionMutation = (
  { __typename?: 'Mutation' }
  & { CreateReaction?: Maybe<(
    { __typename?: 'Reaction' }
    & Pick<Reaction, 'title'>
  )> }
);

export type CreateProjectMutationVariables = Exact<{
  title: Scalars['String'];
  description: Scalars['String'];
  idUser: Scalars['String'];
}>;


export type CreateProjectMutation = (
  { __typename?: 'Mutation' }
  & { CreateProject?: Maybe<(
    { __typename?: 'Project' }
    & Pick<Project, 'title' | 'description'>
  )> }
);

export type ListOpinionsQueryVariables = Exact<{
  by: Scalars['String'];
  id: Scalars['String'];
}>;


export type ListOpinionsQuery = (
  { __typename?: 'Query' }
  & { ListOpinions?: Maybe<Array<Maybe<(
    { __typename?: 'Opinion' }
    & Pick<Opinion, 'title' | 'description'>
  )>>> }
);

export type ListProjectsQueryVariables = Exact<{
  by: Scalars['String'];
  id: Scalars['String'];
}>;


export type ListProjectsQuery = (
  { __typename?: 'Query' }
  & { ListProjects?: Maybe<Array<Maybe<(
    { __typename?: 'ListsProject' }
    & Pick<ListsProject, 'ID_user' | 'id_project'>
  )>>> }
);

export type ProvideUserQueryVariables = Exact<{
  code?: Maybe<Scalars['String']>;
  state?: Maybe<Scalars['String']>;
  provider?: Maybe<Scalars['String']>;
}>;


export type ProvideUserQuery = (
  { __typename?: 'Query' }
  & { provideUser?: Maybe<(
    { __typename?: 'User' }
    & Pick<User, 'id' | 'firstname' | 'lastname' | 'bio'>
  )> }
);


export const CreateOpinionDocument = gql`
    mutation CreateOpinion($title: String!, $description: String!, $idUser: String!, $idProject: String!) {
  CreateOpinion(title: $title, description: $description, idUser: $idUser, idProject: $idProject) {
    title
    description
  }
}
    `;
export type CreateOpinionMutationFn = ApolloReactCommon.MutationFunction<CreateOpinionMutation, CreateOpinionMutationVariables>;
export type CreateOpinionComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<CreateOpinionMutation, CreateOpinionMutationVariables>, 'mutation'>;

    export const CreateOpinionComponent = (props: CreateOpinionComponentProps) => (
      <ApolloReactComponents.Mutation<CreateOpinionMutation, CreateOpinionMutationVariables> mutation={CreateOpinionDocument} {...props} />
    );
    
export type CreateOpinionProps<TChildProps = {}, TDataName extends string = 'mutate'> = {
      [key in TDataName]: ApolloReactCommon.MutationFunction<CreateOpinionMutation, CreateOpinionMutationVariables>
    } & TChildProps;
export function withCreateOpinion<TProps, TChildProps = {}, TDataName extends string = 'mutate'>(operationOptions?: ApolloReactHoc.OperationOption<
  TProps,
  CreateOpinionMutation,
  CreateOpinionMutationVariables,
  CreateOpinionProps<TChildProps, TDataName>>) {
    return ApolloReactHoc.withMutation<TProps, CreateOpinionMutation, CreateOpinionMutationVariables, CreateOpinionProps<TChildProps, TDataName>>(CreateOpinionDocument, {
      alias: 'createOpinion',
      ...operationOptions
    });
};
export type CreateOpinionMutationResult = ApolloReactCommon.MutationResult<CreateOpinionMutation>;
export type CreateOpinionMutationOptions = ApolloReactCommon.BaseMutationOptions<CreateOpinionMutation, CreateOpinionMutationVariables>;
export const CreateReactionDocument = gql`
    mutation CreateReaction($title: String!, $idUser: String!, $idProject: String!) {
  CreateReaction(title: $title, idUser: $idUser, idProject: $idProject) {
    title
  }
}
    `;
export type CreateReactionMutationFn = ApolloReactCommon.MutationFunction<CreateReactionMutation, CreateReactionMutationVariables>;
export type CreateReactionComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<CreateReactionMutation, CreateReactionMutationVariables>, 'mutation'>;

    export const CreateReactionComponent = (props: CreateReactionComponentProps) => (
      <ApolloReactComponents.Mutation<CreateReactionMutation, CreateReactionMutationVariables> mutation={CreateReactionDocument} {...props} />
    );
    
export type CreateReactionProps<TChildProps = {}, TDataName extends string = 'mutate'> = {
      [key in TDataName]: ApolloReactCommon.MutationFunction<CreateReactionMutation, CreateReactionMutationVariables>
    } & TChildProps;
export function withCreateReaction<TProps, TChildProps = {}, TDataName extends string = 'mutate'>(operationOptions?: ApolloReactHoc.OperationOption<
  TProps,
  CreateReactionMutation,
  CreateReactionMutationVariables,
  CreateReactionProps<TChildProps, TDataName>>) {
    return ApolloReactHoc.withMutation<TProps, CreateReactionMutation, CreateReactionMutationVariables, CreateReactionProps<TChildProps, TDataName>>(CreateReactionDocument, {
      alias: 'createReaction',
      ...operationOptions
    });
};
export type CreateReactionMutationResult = ApolloReactCommon.MutationResult<CreateReactionMutation>;
export type CreateReactionMutationOptions = ApolloReactCommon.BaseMutationOptions<CreateReactionMutation, CreateReactionMutationVariables>;
export const CreateProjectDocument = gql`
    mutation CreateProject($title: String!, $description: String!, $idUser: String!) {
  CreateProject(title: $title, description: $description, idUser: $idUser) {
    title
    description
  }
}
    `;
export type CreateProjectMutationFn = ApolloReactCommon.MutationFunction<CreateProjectMutation, CreateProjectMutationVariables>;
export type CreateProjectComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<CreateProjectMutation, CreateProjectMutationVariables>, 'mutation'>;

    export const CreateProjectComponent = (props: CreateProjectComponentProps) => (
      <ApolloReactComponents.Mutation<CreateProjectMutation, CreateProjectMutationVariables> mutation={CreateProjectDocument} {...props} />
    );
    
export type CreateProjectProps<TChildProps = {}, TDataName extends string = 'mutate'> = {
      [key in TDataName]: ApolloReactCommon.MutationFunction<CreateProjectMutation, CreateProjectMutationVariables>
    } & TChildProps;
export function withCreateProject<TProps, TChildProps = {}, TDataName extends string = 'mutate'>(operationOptions?: ApolloReactHoc.OperationOption<
  TProps,
  CreateProjectMutation,
  CreateProjectMutationVariables,
  CreateProjectProps<TChildProps, TDataName>>) {
    return ApolloReactHoc.withMutation<TProps, CreateProjectMutation, CreateProjectMutationVariables, CreateProjectProps<TChildProps, TDataName>>(CreateProjectDocument, {
      alias: 'createProject',
      ...operationOptions
    });
};
export type CreateProjectMutationResult = ApolloReactCommon.MutationResult<CreateProjectMutation>;
export type CreateProjectMutationOptions = ApolloReactCommon.BaseMutationOptions<CreateProjectMutation, CreateProjectMutationVariables>;
export const ListOpinionsDocument = gql`
    query ListOpinions($by: String!, $id: String!) {
  ListOpinions(by: $by, id: $id) {
    title
    description
  }
}
    `;
export type ListOpinionsComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<ListOpinionsQuery, ListOpinionsQueryVariables>, 'query'> & ({ variables: ListOpinionsQueryVariables; skip?: boolean; } | { skip: boolean; });

    export const ListOpinionsComponent = (props: ListOpinionsComponentProps) => (
      <ApolloReactComponents.Query<ListOpinionsQuery, ListOpinionsQueryVariables> query={ListOpinionsDocument} {...props} />
    );
    
export type ListOpinionsProps<TChildProps = {}, TDataName extends string = 'data'> = {
      [key in TDataName]: ApolloReactHoc.DataValue<ListOpinionsQuery, ListOpinionsQueryVariables>
    } & TChildProps;
export function withListOpinions<TProps, TChildProps = {}, TDataName extends string = 'data'>(operationOptions?: ApolloReactHoc.OperationOption<
  TProps,
  ListOpinionsQuery,
  ListOpinionsQueryVariables,
  ListOpinionsProps<TChildProps, TDataName>>) {
    return ApolloReactHoc.withQuery<TProps, ListOpinionsQuery, ListOpinionsQueryVariables, ListOpinionsProps<TChildProps, TDataName>>(ListOpinionsDocument, {
      alias: 'listOpinions',
      ...operationOptions
    });
};
export type ListOpinionsQueryResult = ApolloReactCommon.QueryResult<ListOpinionsQuery, ListOpinionsQueryVariables>;
export const ListProjectsDocument = gql`
    query ListProjects($by: String!, $id: String!) {
  ListProjects(byIdCategory: $by, id: $id) {
    ID_user
    id_project
  }
}
    `;
export type ListProjectsComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<ListProjectsQuery, ListProjectsQueryVariables>, 'query'> & ({ variables: ListProjectsQueryVariables; skip?: boolean; } | { skip: boolean; });

    export const ListProjectsComponent = (props: ListProjectsComponentProps) => (
      <ApolloReactComponents.Query<ListProjectsQuery, ListProjectsQueryVariables> query={ListProjectsDocument} {...props} />
    );
    
export type ListProjectsProps<TChildProps = {}, TDataName extends string = 'data'> = {
      [key in TDataName]: ApolloReactHoc.DataValue<ListProjectsQuery, ListProjectsQueryVariables>
    } & TChildProps;
export function withListProjects<TProps, TChildProps = {}, TDataName extends string = 'data'>(operationOptions?: ApolloReactHoc.OperationOption<
  TProps,
  ListProjectsQuery,
  ListProjectsQueryVariables,
  ListProjectsProps<TChildProps, TDataName>>) {
    return ApolloReactHoc.withQuery<TProps, ListProjectsQuery, ListProjectsQueryVariables, ListProjectsProps<TChildProps, TDataName>>(ListProjectsDocument, {
      alias: 'listProjects',
      ...operationOptions
    });
};
export type ListProjectsQueryResult = ApolloReactCommon.QueryResult<ListProjectsQuery, ListProjectsQueryVariables>;
export const ProvideUserDocument = gql`
    query ProvideUser($code: String, $state: String, $provider: String) {
  provideUser(code: $code, state: $state, provider: $provider) {
    id
    firstname
    lastname
    bio
  }
}
    `;
export type ProvideUserComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<ProvideUserQuery, ProvideUserQueryVariables>, 'query'>;

    export const ProvideUserComponent = (props: ProvideUserComponentProps) => (
      <ApolloReactComponents.Query<ProvideUserQuery, ProvideUserQueryVariables> query={ProvideUserDocument} {...props} />
    );
    
export type ProvideUserProps<TChildProps = {}, TDataName extends string = 'data'> = {
      [key in TDataName]: ApolloReactHoc.DataValue<ProvideUserQuery, ProvideUserQueryVariables>
    } & TChildProps;
export function withProvideUser<TProps, TChildProps = {}, TDataName extends string = 'data'>(operationOptions?: ApolloReactHoc.OperationOption<
  TProps,
  ProvideUserQuery,
  ProvideUserQueryVariables,
  ProvideUserProps<TChildProps, TDataName>>) {
    return ApolloReactHoc.withQuery<TProps, ProvideUserQuery, ProvideUserQueryVariables, ProvideUserProps<TChildProps, TDataName>>(ProvideUserDocument, {
      alias: 'provideUser',
      ...operationOptions
    });
};
export type ProvideUserQueryResult = ApolloReactCommon.QueryResult<ProvideUserQuery, ProvideUserQueryVariables>;