import React, { useState, Fragment } from "react";
import RichTextEditor from "react-rte";
import ReactMarkdown from "react-markdown";

interface PreviewBlockInterface {
  body: string
}

function PreviewBlock(props: PreviewBlockInterface): React.ReactElement {
  const [markdownContent, setMarkdownContent]: any = useState(" Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla accumsan, metus ultrices eleifend gravida, nulla nunc varius lectus, nec rutrum justo nibh eu lectus. Ut vulputate semper dui. Fusce erat odio, sollicitudin vel erat vel, interdum mattis neque.");
  return (
    <Fragment>
        <ReactMarkdown source={markdownContent} />
    </Fragment>
  );
}

export default PreviewBlock;
