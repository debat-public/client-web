import React, { useState, useEffect } from "react";
import RichTextEditor from "react-rte";

interface EditBlockInterface {
    title: string;
    mardownContent: string;
    show: boolean;
    uid?: string;
    onHide(): void;
}

function EditBlock(props: EditBlockInterface): React.ReactElement {
    const [value, setValue]: any = useState(RichTextEditor.createValueFromString(props.mardownContent, 'markdown'));
    const [open, setOpen] = useState("");
    const [markdownContent, setMarkdownContent]: any = useState(props.mardownContent);

    useEffect(() => {
        if (props.show === true) {
            setOpen("is-active");
        }
    }, [props.show])

    const close = () => {
        props.onHide();
        setOpen("");
    }

    const save = () => {
        close();
    }

    const onChange = (value: any) => {
        setValue(value);
        setMarkdownContent(value.toString("markdown"));
    };

    return (
        <div className={`modal ${open}`}>
            <div className="modal-background"></div>
            <div className="modal-card">
                <header className="modal-card-head">
                    <p className="modal-card-title">{props.title}</p>
                    <button className="delete" aria-label="close" onClick={() => close()}>Close</button>
                </header>
                <section className="modal-card-body">
                    <RichTextEditor
                        value={value}
                        onChange={onChange}
                        placeholder={"Décrivez votre projet"}
                    />
                </section>
                <footer className="modal-card-foot">
                    <button className="button is-primary" onClick={() => save()}>Save changes</button>
                    <button className="button" onClick={() => close()}>Cancel</button>
                </footer>
            </div>
        </div>
    )
}

export default EditBlock;