import React, { useState, Fragment } from "react";
import RichTextEditor from "react-rte";
import ReactMarkdown from "react-markdown";

interface DraftInterface {}

function DraftProject(props: DraftInterface): React.ReactElement {
  const [value, setValue]: any = useState(RichTextEditor.createEmptyValue());
  const [markdownContent, setMarkdownContent]: any = useState("");

  const onChange = (value: any) => {
    setValue(value);
    setMarkdownContent(value.toString("markdown"));
  };

  return (
    <Fragment>
      <div style={{ width: "80%", margin: "auto" }}>
        <RichTextEditor
          value={value}
          onChange={onChange}
          placeholder={"Décrivez votre projet"}
        />
      </div>
      <div>{JSON.stringify(markdownContent)}</div>
    </Fragment>
  );
}

export default DraftProject;
