import { store as notification } from 'react-notifications-component';

function Notify(
  title: string, 
  message: string, 
  type: "success" | "danger" | "info" | "default" | "warning"
) {
  notification.addNotification({
    title, message, type,
    // let's keep this part common for the whole app
    // only configurable in this file
    insert: "bottom",
    container: "bottom-right",
    animationIn: ["animated", "fadeIn"],
    animationOut: ["animated", "fadeOut"],
    dismiss: {
      duration: 5000,
      pauseOnHover: true,
      onScreen: true
    }
  });
}
export default Notify;