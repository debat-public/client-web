import React, { useState } from "react";
import { Link } from "react-router-dom";

function ListCategories() {
  const selectInitial = window.location.search.split("=")[1];
  const [selectedItem, setItem] = useState(selectInitial);
  const items = ["Budget", "Culture", "Defense", "Environnement"];
  return (
    <aside className="menu" style={{padding:"1.5rem"}}>
      <p className="menu-label">
        General
        </p>
      <ul className="menu-list">
        {items.map((el: string, i: number) => (
          <li key={`item-${i}`}>
            <Link
              to={`/?item=${el}`}
              className={i > 0 ? (selectedItem === el ? "active" : "") : (!selectedItem || selectedItem === el ? "active" : "")}
              onClick={() => setItem(el)}
            >
              {el}
            </Link>
          </li>
        ))}
      </ul>
    </aside>
  );
}
export default ListCategories;
