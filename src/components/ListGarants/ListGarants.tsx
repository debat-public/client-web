import React, { useState } from "react";
import { useQuery } from "@apollo/react-hooks";
import listGarantsJSON from "../../json/listgarants.json";
import ProfilePreview from "../Profile/ProfilePreview";
import { GET_GARANTS } from "../../graphql/queries";

/**
 * Component LisGarants
 */
function ListGarants(): React.ReactElement {
  const { loading, error, data } = useQuery(GET_GARANTS);
  console.table({ loading, error, data });
  const [listGarants, setListGarants] = useState<Array<any>>(
    data?.ListGarants || listGarantsJSON
  );

  function loadMore() {
    console.log("load more");
    setListGarants([...listGarants, ...listGarantsJSON]);
  }

  return (
    <div className="">
      {listGarants.map(
        (
          e: { firstName: string; lastName: string; bio: string; job: string },
          i: number
        ) => (
          <ProfilePreview
            firstName={e.firstName}
            lastName={e.lastName}
            job={e.job}
            bio={e.bio}
            key={i}
          />
        )
      )}
      <div style={{ textAlign: "center", margin: "2rem 0" }}>
        <button className="button is-primary" onClick={loadMore}>
          Plus de garants
        </button>
      </div>
    </div>
  );
}
export default ListGarants;
