import React, { ReactElement } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import User from "../../interfaces/User.interface";
import Notify from "../Notify";
import * as Scroll from "react-scroll";
import Tour from "reactour";

// let Link      = Scroll.Link;
// let Element   = Scroll.Element;
// let Events    = Scroll.Events;

interface Props {
  user?: User;
}

function Navbar(props: Props): ReactElement {
  const { user } = props;
  const [isActive, setisActive] = React.useState(false);

  const logout = () => {
    Notify(
      "Déconnexion réussie",
      "Vous serez redirigé dans quelques instants",
      "success"
    );
    localStorage.removeItem("user");
    localStorage.removeItem("code");
    setTimeout(() => window.location.reload(), 1500);
  };
  return (
      <nav
        className="navbar is-fixed-top has-shadow"
        role="navigation"
        aria-label="main navigation"
      >
        <div className="navbar-brand">
          <Scroll.Link
            className="navbar-item"
            to="top"
            spy={true}
            smooth={true}
            offset={-70}
            duration={500}
          >
            <img
              src="/images/logo-marianne.svg"
              alt="Logo de marianne"
              width="112"
              height="28"
            />
            <span> Débat public</span>
          </Scroll.Link>
          <span
            onClick={() => {
              setisActive(!isActive);
            }}
            role="button"
            className={`navbar-burger burger ${isActive ? "is-active" : ""}`}
            aria-label="menu"
            aria-expanded="false"
            data-target="navbarBasicExample"
          >
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
          </span>
        </div>
        <div
          id="navbarBasicExample"
          className={`navbar-menu ${isActive ? "is-active" : ""}`}
        >
          <div className="navbar-end">
            <a className="navbar-item" href="/">
              Accueil
            </a>
            <a className="navbar-item" href="/project/new">
              Proposer un projet
            </a>

            <a className="navbar-item" href="/garants">
              Nos garants
            </a>

            <a className="navbar-item" href="/documentation">
              Documentation
            </a>

            {user?.id ? (
              [
                <li key={`${Math.random()}`} className="navbar-item">
                  <Link to={"/profile/" + user.id} className="navbar-item">
                    UserID : {user.id}
                  </Link>
                </li>,
                <li key={`${Math.random()}`} className="navbar-item">
                  <button
                    className="button is-primary"
                    onClick={logout}
                    style={{ color: "black", backgroundColor: "white" }}
                  >
                    Logout
                  </button>
                </li>,
              ]
            ) : (
              <li className="navbar-item">
                <div className="buttons">
                  <a className="button is-primary" href="/connexion">
                    <strong>S'identifier</strong>
                  </a>
                  <a className="button is-light" href="/connexion">
                    Se connecter
                  </a>
                </div>
              </li>
            )}
          </div>
        </div>
      </nav>
  );
}
export default connect((store) => store)(Navbar);
