import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { GET_REACTIONS } from "../../graphql/queries";
import { useQuery } from "@apollo/react-hooks";
import PostPreviewInterface from "../../interfaces/ProjectPreview.interface";
import listReactionsJSON from '../../json/listTags.json';

/**
 * Component ProjectPreview
 */
function ProjectPreview(props: PostPreviewInterface): React.ReactElement {
  const { id, title, description, date  } = props;
  const { loading, error, data } = useQuery(GET_REACTIONS);
  const listReactions = data?.ListReactions || listReactionsJSON;
  const history = useHistory();

  return (
    <div className="card" style={{margin: "1.25rem"}} onClick={() => { history.push(`/project/${id}`); }} >
      <header className="card-header">
        <p className="card-header-title">
          {/* Projet Cigéo - bilan du garant de la concertation */}
          { title }
        </p>
        <p className="card-content">Crée le {date}</p>
        <div className="card-header-icon" aria-label="more options">
          <span className="icon">
            <i className="fas fa-angle-down" aria-hidden="true"></i>
          </span>
        </div>
      </header>
      <div className="card-content">
        <div className="content">
          { description }

        </div>
      </div>
      <footer className="card-footer card-content">
        <div className="tags">
        {listReactions.map(
            (tag: { name: string, ref: string}, i: number) => (
              <div className="tag" key={`tag-${i}`}>{tag.name}</div>
            )          
          )}
        </div>
      </footer>
    </div>
  );
}
export default ProjectPreview;
