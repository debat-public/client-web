import React, { useState } from "react";
import ProjectPreview from "./ProjectPreview";
import { useQuery } from "@apollo/react-hooks";
import listProjectsJSON from "../../json/listprojects.json";
import { GET_PROJECTS } from '../../graphql/queries';

/**
 * Component LisProjects
 */
function ListProjects(): React.ReactElement {
  const { loading, error, data } = useQuery(GET_PROJECTS);
  console.log(data);
  const [listProjects, setListProjects] = useState<Array<any>>(data?.ListProjects || listProjectsJSON);

  function loadMore() {
    console.log('load more');
    // if (data) {
      // do something with graphql
    // } else {
      // TODO
      setListProjects([...listProjects, ...listProjectsJSON]);
    // }
  }

  return (
    <div className="">
      {listProjects.map(
        (e: { title: string; description: string, date: string}, i: number) => (
          <ProjectPreview date={e.date} title={e.title} description={e.description} key={i} />
        )
      )}
      <div style={{ textAlign: "center", margin: "2rem 0"}}>
        <button
          className="button is-primary"
          onClick={loadMore}
        >
          Projets suivants
        </button>
      </div>
    </div>
  );
}
export default ListProjects;
