import React from "react";

interface FilePreviewInterface {
    title: String;
}

function FilePreview(props: FilePreviewInterface): React.ReactElement {
    const { title } = props;

    return (
        <div>
            <div>
                {title}
            </div>
        </div>
    );
}

export default FilePreview;
