import React from "react";

interface OpinionPreviewInterface {
  id?: string;
  title: string;
  description: string;
}

function OpinionPreview(props: OpinionPreviewInterface): React.ReactElement {
    const { id, title, description } = props;

  return (
    <article className="media has-background-white">
      <figure className="media-left">
        <p className="image is-48x48">
          <img
            alt="profile"
            src="https://picsum.photos/96/96"
          />
        </p>
      </figure>
      <div className="media-content">
        <div className="content">
          <p>
            <strong>{title}</strong>
            <br />
            <br />
            {description}
            <br />
            <small>
              <a href="/#">Like</a>
            </small>
          </p>
        </div>
      </div>
    </article>
  );
}
export default OpinionPreview;
