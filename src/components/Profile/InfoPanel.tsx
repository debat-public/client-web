import React from "react";
import ListProjects from "../ListProjects/ListProjects";
import ListOpinions from "../ListOpinions/ListOpinions";
import MenuReaction from "../Project/MenuReaction";
import listReactionsJSON from "../../json/listreactions.json";


interface ContentProfileInterface {
  firstName: String;
  lastName: String;
  age?: Number;
  job?: String;
  bio?: String;
}

export enum StatusKey {
  DEFAULT = "Default",
  REACTIONS = "Reactions",
  OPINION = "Opinions",
  PROJECTS = "Projects",      
}

interface InfoPanelInterface {
  statusKey: StatusKey;
}

function ContentProfile(props: ContentProfileInterface): React.ReactElement {
  const { firstName, lastName, age, job, bio } = props;

  return (
    <div className="content">
      <p>Bio: {bio}</p>
      <p>Age: 26 ans Ville: Paris 75017 Profession: Développeur informatique</p>
      <div className="collapse">
        <h4> Activité actuelle et employeur: </h4>
        <div className="collapse-content">
          <p>Informations</p>
        </div>
      </div>
      <div className="collapse">
        <h4> Domaines d’intérêt: </h4>

        <div className="collapse-content">
          <p>Informations</p>
        </div>
      </div>
    </div>
  );
}


function InfoPanel(props: InfoPanelInterface) {
  const selectPanel = (statusKey: StatusKey) => {
    console.log(statusKey)
    const map: { [key: string]: any } = {
      Projects: <ListProjects />,
      Reactions: <MenuReaction  listReactions={listReactionsJSON} title="Réactions" />,
      Opinions: <ListOpinions />,
      Default: <ContentProfile firstName="dennis" lastName="Henri" />,
    };
    return  map[statusKey]; 
  };
  const { statusKey } = props;
  console.log(selectPanel(statusKey))
  return selectPanel(statusKey);
}


export default InfoPanel;