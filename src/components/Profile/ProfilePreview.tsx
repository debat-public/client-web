import React from "react";
import ProfilePreviewInterface from "../../interfaces/ProfilePreview.interface";
import { useHistory } from "react-router-dom";

/**
 * Component ProfilePreview
 */

const date = "3 Octobre 2020";

function ProfilePreview(props: ProfilePreviewInterface): React.ReactElement {
  const { id, lastName, firstName, bio } = props;
  const history = useHistory();
  return (
    <div className="box is-primary" style={{ margin: "1.25rem" }} onClick={() => { history.push(`/profile/${id}`); }}>
      <article className="media" >
        <div className="media-left">
          <figure className="image is-64x64">
          <img
            className="is-rounded"
            src="https://picsum.photos/96/96"
            alt="Placeholder"
          />
          </figure>
        </div>
        <div className="media-content">
          <div className="content">
            <p>
              <strong>{lastName + " " + firstName}</strong> <small>@{id}</small>
              <br />
              {bio}
            </p>
          </div>
          <nav className="level is-mobile">
            <div className="level-left">
              <a className="level-item" aria-label="reply">
                <span className="icon is-small">
                  <i className="fas fa-reply" aria-hidden="true"></i>
                </span>
              </a>
              <a className="level-item" aria-label="retweet">
                <span className="icon is-small">
                  <i className="fas fa-retweet" aria-hidden="true"></i>
                </span>
              </a>
              <a className="level-item" aria-label="like">
                <span className="icon is-small">
                  <i className="fas fa-heart" aria-hidden="true"></i>
                </span>
              </a>
            </div>
          </nav>
        </div>
  <div className="media-right">
    <span> test</span>
  </div>
      </article>
    </div>
  );
}
export default ProfilePreview;
