import React from "react";
import { StatusKey } from "./InfoPanel";
interface MenuProfileInteface {
  statusKey: StatusKey;
}

function MenuProfile(props: MenuProfileInteface): React.ReactElement {
  const { statusKey } = props;
  return (
    <div>
      <div className="container is-flex">
        {/* <div className="container is-flex" style={"justify-content": "center"}> */}
        <figure className="image is-96x96">
          <img
            className="is-rounded"
            src="https://picsum.photos/96/96"
            alt="Placeholder"
          />
        </figure>
      </div>
      <p className="title is-4"></p>
      {/* <p className="subtitle is-6">@{firstname.toLowerCase()}{lastname.toLowerCase()}</p> */}

      <div className="tabs is-centered">
        <ul>
          <li className={( statusKey == "Default") ? "is-active" : "" }>
            <a href="?panel=Default">A propos</a>
          </li>
          <li className={( statusKey == "Opinions") ? "is-active" : "" }>
            <a href="?panel=Opinions">Opinions</a>
          </li>
          <li className={( statusKey == "Reactions") ? "is-active" : "" }>
            <a href="?panel=Reactions">Réactions</a>
          </li>
          <li className={( statusKey == "Projects") ? "is-active" : "" }>
            <a href="?panel=Projects">Publications</a>
          </li>
        </ul>
      </div>
    </div>
  );
}

export default MenuProfile;
