import User from '../../interfaces/User.interface';

interface Input {
  type: string;
  data: any;
}
const userReducer = (state: User = {}, { type , data }: Input) => { 
  switch(type){
    case 'SET_USER':
      localStorage.setItem('user', JSON.stringify(data));
      state = data;
      break;
    default:
      return state;
  }
  return state;
}
export default userReducer;