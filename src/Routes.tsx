import React from "react";
import { Switch, Route } from "react-router";
import {
  HomePage,
  TestQueryPage,
  ConnexionPage,
  ProjectPage,
  ProfilePage,
  NotFoundPage,
  Loading,
  DraftPage,
  NosGarantsPage,
  France
} from "./views";

function Routes() {
  return (
    <Switch>
      <Route exact path="/" component={HomePage} />
      <Route
        exact
        path="/carte-deparementale"
        component={() => (
          <France id={"carte-de-france"} width={700} height={500} />
        )}
      />
      <Route path="/france/:id" component={() => <div/>} />
      <Route exact path="/connexion" component={ConnexionPage} />
      <Route exact path="/draft" component={DraftPage} />
      <Route exact path="/project/:id" component={ProjectPage} />
      <Route exact path="/profile/:id" component={ProfilePage} />
      <Route exact path="/loading" component={Loading} />
      <Route exact path="/garants" component={NosGarantsPage} />
      <Route exact path="/test/query" component={TestQueryPage} />
      <Route exact path="*" component={NotFoundPage} />
    </Switch>
  );
}
export default Routes;
