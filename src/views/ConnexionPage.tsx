import React, { useState } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import Notify from "../components/Notify";
import Login from "../components/Connexion/Login";

const OAUTH2_42_REDIRECT = process.env.REACT_APP_OAUTH2_42_REDIRECT; //"https://api.intra.42.fr/oauth/authorize?client_id=9bebbad07225b94bd4346d1ad0a2e00804db403b3d64ace1933a05c38904b5d8&redirect_uri=http%3A%2F%2Flocalhost%3A3000&response_type=code";
interface Props {
  dispatch: Function;
}
function connexion(dispatch: Function, setRedirect: Function, email: string) {
  dispatch({
    type: "SET_USER",
    data: {
      id: email,
    },
  });
  Notify(
    "Connexion réussie",
    "Vous êtes maintenant connecté avec l'email " + email,
    "success"
  );
  setRedirect(true);
}

function ConnexionPage(props: Props): React.ReactElement {
  const [redirect, setRedirect] = useState(false);
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { dispatch } = props;
  return (
    <div className="container">
      <div style={{ marginTop: "100px" }}>
        <h1>Accéder à mon Compte</h1>
        <hr />
        <Login />
        {redirect ? <Redirect to="/" /> : ""}
        <hr />
        <button
          className="button"
          style={{ float: "right", marginRight: 0 }}
          onClick={() => connexion(dispatch, setRedirect, "email")}
        >
          Se Connecter
        </button>
      </div>
      <hr />
      <div>
        <a href={OAUTH2_42_REDIRECT}>
          <button
            className="button"
            style={{
              backgroundColor: "white",
              color: "black",
              border: "solid 1px",
              fontSize: "22px",
              width: "100%",
            }}
          >
            Se Connecter avec
            <img
              alt="42"
              src="https://ublu.fr/wp-content/uploads/2018/04/Logo-42.png"
              style={{
                marginLeft: "10px",
                width: "30px",
              }}
            />
          </button>
        </a>
      </div>
    </div>
  );
}
export default connect((s) => s)(ConnexionPage);
