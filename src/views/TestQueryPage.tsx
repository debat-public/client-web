import React from "react";
import ListCategories from "../components/Home/ListCategories";
import ListProjects from "../components/ListProjects/ListProjects";
import ListFiles from "../components/ListFiles/ListFiles";

function TestQueryPage(): React.ReactElement {
  return (
    <section className="section">
      <div className="hero">
        <div className="hero-body">
          <h1 className="title has-text-centered">
            Test Query Page
          </h1>
        </div>
        <ListCategories />
        <hr/>
        <ListProjects />
        <hr/>
        <ListFiles />
      </div>
      <div className="container is-fluid is-flex">
      </div>
    </section>
  );
}

export default TestQueryPage;
