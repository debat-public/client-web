import HomePage from "./HomePage";
import ConnexionPage from "./ConnexionPage";
import ProjectPage from "./ProjectPage";
import ProfilePage from "./ProfilePage";
import NosGarantsPage from "./NosGarantsPage";
import NotFoundPage from "./NotFoundPage";
import DraftPage from "./DraftPage";
import TestQueryPage from "./TestQueryPage";

import France from "./France";

import Loading from "./Loading";


export {
  HomePage,
  France,
  ConnexionPage,
  DraftPage,
  ProjectPage,
  ProfilePage,
  NotFoundPage,
  NosGarantsPage,
  Loading,
  TestQueryPage,
};
