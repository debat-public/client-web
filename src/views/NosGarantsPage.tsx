import React from "react";
import ListGarants from "../components/ListGarants/ListGarants";


export const steps = [
  {
    selector: ".title",
    content: "test",
  },
  {
    selector: ".container",
    content: "test",
  },

  // ...
];


// Leur rôle est de garantir :

// la qualité, l'intelligibilité et la sincérité des informations
//   diffusées ;

// les modalités de participation du public ;</li>

//   le contenu et la qualité des outils d'information et de
//   communication ;

//   les caractéristiques et la pertinence des outils d'expression du
//   public ;

//   la possiblité pour le public de poser des questions et d'obtenir
//   des réponses appropriées de la part du maître d'ouvrage ;

//   la possibilité d'exprimer des points de vue structurés sur le
//   projet, et que ces points de vue soient pris en compte.

//   Les garants doivent faire preuve de neutralité et sont tenus de
// signer une charte d'éthique et de déontologie au début de chaque
// mission. Enfin, les garants veillent au respect des principes du
// débat public pendant toute la durée de leur mission :
// <li>Débattre sur l’opportunité du projet.</li>
// <li>Donner le même poids à l’opinion de chacun.</li>
// <li>
//   Veiller au respect des bonnes conditions d’information du
//   public.
// </li>
// <li>
//   Veiller à ce que le maître d’ouvrage s’abstienne de toute
//   décision pendant le débat public.
// </li>
// <li>Rendre compte des décisions prises après le débat public.</li>
// A l'issue de la concertation, les garants établissent un bilan
// comportant une synthèse des observations et des propositions
// présentées. Le bilan est rendu public sur le site de la CNDP et
// est joint au dossier d'enquête publique.
// </h3>

function NosGarantsPage(): React.ReactElement {
  return (
    <div>
      <section className="section has-background-grey-lighter">
        <div className="hero">
          <div className="hero-body">
            <h1 className="title has-text-centered" id="top">
              Nos Garants de debat.gouv.fr
            </h1>
            <h3 className="has-text-centered" style={{padding: "0 7rem"}} >
              Les 250 garants inscrits sur la liste nationale de la CNDP ont été
              sélectionnés et formés pour garantir les procédures de
              concertation obligatoires ou facultatives sur l'ensemble du
              territoire national.
            </h3>
          </div>
        </div>
      </section>
      <section className="section">
        <ListGarants />
      </section>
    </div>
  );
}
export default NosGarantsPage;
