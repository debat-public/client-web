import gql from 'graphql-tag';

/**
 *  Queries 
 */
export const GET_BLOCKS = gql`
query ListBlocks($code: String, $state: String, $provider: String) {
  ListBlocks {
    id
    firstname
    lastname
    bio
  }
}
`;

export const GET_FILES =  gql`
query ListBlocks($code: String, $state: String, $provider: String) {
  ListBlocks {
    id
    firstname
    lastname
    bio
  }
}
`;

export const GET_GARANTS =  gql`
query ListBlocks($code: String, $state: String, $provider: String) {
  ListBlocks {
    id
    firstname
    lastname
    bio
  }
}
`;


export const GET_REACTIONS =  gql`
query ListBlocks($code: String, $state: String, $provider: String) {
  ListBlocks {
    id
    firstname
    lastname
    bio
  }
}
`;

export const GET_OPINIONS = gql`
query ListOpinions($by: String!, $id: String!){
  ListOpinions(by: $by, id: $id) {
    title
    description
  }
}
`;

export const GET_PROJECTS = gql`
query ListProjects($by: String!, $id: String!) {
  ListProjects(byIdCategory: $by, id: $id) {
    ID_user
    id_project
  }
}
`;

// export const GET_USER_PROFILE = gql`
// query User  {
//   user {
//     firstname
//     lastname
//     age
//     job
//     bio
//   }
// }
// `;

export const GET_USER = gql`
query ProvideUser($code: String, $state: String, $provider: String) {
  provideUser(code: $code, state: $state, provider: $provider) {
    id
    firstname
    lastname
    bio
  }
}
`;