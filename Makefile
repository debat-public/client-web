#################################################################
#     CSS 																
#################################################################

import_css:
	git clone https://gitlab.com/debat-public/css-debat-public src/css/de

build_css:
	npm run build --prefix src/css


#############################################################
#   React
###############################################################
# This is just for specific developpement, see infra project to launch the all project 

build_prod:
	@echo  "be patient"

run:
	npm start


################################################################################
#   DOCKER
################################################################################

# Reload the container in docker-compose. See infra project
docker_compose_reload:
	docker-compose -f  ../infra/dev/docker-compose.yaml restart  client-web

# Build 
docker_build:
	docker build -t registry.gitlab.com/debat-public/client-web .

# Build and launch Docker with the app
docker_run: docker_stop docker_rm docker_build
	docker run -d -p 3000:3000 -v $(PWD):/app --name client-web_container -t -e REACT_APP_SERVER_URL=http://localhost:8383 registry.gitlab.com/debat-public/client-web:latest

docker_exec_shell:
	docker exec -it client-web_container /bin/sh

# Log
docker_log:
	docker logs -f client-web_container

# Stop container
docker_stop:
	docker stop client-web_container || true

# Remove container
docker_rm:
	docker rm client-web_container || true

# Stop all docker containers
docker_fstop:
	docker stop $(docker ps -a -q)

# Build and Push the image to the gitlab registry
docker_push: docker_build docker_login
	docker push registry.gitlab.com/debat-public/client-web

# Login docker with registry.gitlab.com
docker_login:
	docker login registry.gitlab.com

# Remove all docker containers and images from the machine (add -f to force delete)
docker_fclean:
	docker rm $(docker ps -a -q)
	docker rmi $(docker images -q)
